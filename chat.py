import random
import socket
import sys
import threading
import time

class Server:

    addresses = []

    connections = []

    peers = []

    def __init__(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(('0.0.0.0', 10000))
        sock.listen(1)

        print('Server running...')

        while True:
            c, a = sock.accept()

            ct = threading.Thread(target=self.handler, args= (c, a))
            ct.daemon = True
            ct.start()

            self.addresses.append(a)
            self.connections.append(c)
            self.peers.append(a[0])

            print(str(a[0]) + ':' + str(a[1]), 'connected')

            self.send_peers()

    def handler(self, c, a):
        while True:
            data = c.recv(1024)

            for connection in self.connections:
                connection.send(data)

            if not data:
                print(str(a[0]) + ':' + str(a[1]), 'disconnected')
                self.addresses.remove(a)
                self.connections.remove(c)
                self.peers.remove(a[0])
                c.close()

                self.send_peers()

                break

    def send_peers(self):
        p = ''

        for peer in self.peers:
            p = p + peer + ','

        for connection in self.connections:
            connection.send(b'\x11' + bytes(p, 'utf-8'))

class Client:


    def __init__(self, address):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        sock.connect((address, 10000))

        it = threading.Thread(target=self.send_message, args=(sock,))
        it.daemon = True
        it.start()

        while True:
            data = sock.recv(1024)

            if not data:
                break

            if data[:1] == b'\x11':
                self.update_peers(data[1:])
            else:
                print(str(data, 'utf-8'))

    def send_message(self, sock):
        while True:
            sock.send(bytes(input(), 'utf-8'))

    def update_peers(self, peer_data):
        P2P.peers = str(peer_data, 'utf-8').split(',')[:-1]

class P2P:

    peers = ['127.0.0.1']

def main():
    while True:
        try:
            print('Trying to connect...')

            time.sleep(random.randint(1, 5))

            for peer in P2P.peers:
                try:
                    client = Client(peer)
                except KeyboardInterrupt:
                    sys.exit(0)
                except:
                    pass

                if random.randint(1, 20) == 1:
                    try:
                        server = Server()
                    except KeyboardInterrupt:
                        sys.exit(0)
                    except:
                        print("Couldn't start the server....")

        except KeyboardInterrupt:
            sys.exit(0)

if __name__ == "__main__":
    main()
